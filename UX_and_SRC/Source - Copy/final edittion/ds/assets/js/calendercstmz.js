$('#datepicker').datepicker({
    // clearBtn: true
})
.on('show', function(e) {
    var dsp = $(".datepicker").css('display');
    var next = $(".datepicker-days > table > thead > tr > th.next");
    var prev = $(".datepicker-days > table > thead > tr > th.prev");
    $("#datepicker").addClass('border-primary');
    if (dsp == 'block') {
        $(".datepicker").addClass('border border-primary border-2');
        $(".datepicker").css('padding','30px');
        $(".datepicker").css('margin-top','10px');
        $(".datepicker-switch").addClass('dropdown-toggle border border-secoundary rounded-2');
        $(".datepicker-days > table > tbody").addClass('border border-secoundary border-radiuos rounded-2');
        $(".datepicker-days > table > tbody > tr > td").addClass('border border-secoundary');
        next.before(prev);
        next.empty();
        prev.empty();
        next.append('<i class="bi bi-chevron-right" id="cal-next-btn"></i>');
        prev.append('<i class="bi bi-chevron-left" id="cal-prev-btn"></i>');
    }
    console.log(dsp);
});

$('#datepicker').datepicker({
    // clearBtn: true
}).on('hide',function(e) {
    $("#datepicker").removeClass('border-primary');
});