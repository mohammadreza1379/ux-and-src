$('#btnmagnify').click(function (e) {
    var position = $(this).position();
    var elements = `
    <div style="display: grid;position: absolute;top: `+(position.top)*1.75+`px;left: `+(position.left)*1.39+`px; padding:3px ;" id="magnify">
        <button class="form-control border border-1 border-secondary rounded-1 magnifier-tool-plus" onclick="magnify_plus()">
            <i class="bi bi-plus border-0"></i>
        </button>
        <button class="form-control border border-1 border-secondary rounded-1 magnifier-tool-minus" onclick="magnify_minus()">
            <i class="bi bi-dash border-0"></i>
        </button>
    </div>`;
    if ($("#magnify").length == 0) {
        $('body').append(elements);
    }
    else
    {
        $("#magnify").remove();
    }
});

$('.bi-zoom-in').click(function(e){
    magnify_plus();
});

$('.bi-bookmark-plus').click(function(e){
    alert('save to bookmark');
});

$('.bi-download').click(function(e){
    var crnt = $(this);
    var i = 0;
    do {
        crnt = crnt.parent();
        var src = crnt.find('img').attr('src');
    } while (!(crnt.find('img').length || crnt.find('ul').length));
    if (crnt.find('img').length) {
        if (($(this).parent().find('a')).length) {
            $(this).parent().attr({target: '_blank', href  : src , download: 'image.jpg'});
        }
        else
        {
            $(this).wrap('<a target="_blank" href="'+src+'" download="image.jpg"></a>');
        }
    }
    else if(crnt.find('ul').length)
    {
        alert('download any thing you want');
    }
    
    $(this).parent()[0].click();
})

$('.bi-folder-plus').click(function(e){
    alert('folder');
});

$('.bi-calendar-plus-fill').click(function(){
    alert('clicked on me');
});

$('input[type="checkbox"]').each(function(){
    $(this).wrap(`<div class="example">
    <label class="checkbox-button">
      <input type="checkbox" class="checkbox-button__input" id="choice1-1" name="choice1">
      <span class="checkbox-button__control"></span>
    </label>
  </div>`);
    $(this).remove();

});

function magnify_plus() {
    // updatemagnify_pos('plus');
    var fontSize = parseInt($("#messages").css("font-size")) + 1 + "px";
    var fontSize_desc = parseInt($(".description").css("font-size")) + 1 + "px";
    var fontSize_detail = parseInt($("div[class*='detail']").css("font-size")) + 1 + "px";
    var fontSize_title = parseInt($("div[class*='title']").css('font-size')) + 1 + "px";

    $(".image-title").css({'font-size':fontSize_title});
    $(".image-detail").css({"font-size":fontSize_detail});
    $(".title").css({'font-size':fontSize_title});
    $(".description").css({'font-size':fontSize_desc});
    $("#messages").css({'font-size':fontSize});
    $("#messages").find('*').css({'font-size':fontSize});
}
function magnify_minus()
{
    // updatemagnify_pos('minus');
    var fontSize = parseInt($("#messages").css("font-size"));
    fontSize = fontSize - 1 + "px";
    $("#messages").css({'font-size':fontSize});
    $("#messages").find('*').css({'font-size':fontSize});
}
// function updatemagnify_pos(params) {
//     if (params == 'plus') {
//         var top = parseInt($("#magnify").css('top').replace('px',''));
//         var left = parseInt($("#magnify").css('left').replace('px',''));
//         $("body").css('top',top+3+'px');
//         // $("#magnify").css('left',left+0.5+'px');
//     }
//     else{
//         var top = parseInt($("#magnify").css('top').replace('px',''));
//         var left = parseInt($("#magnify").css('left').replace('px',''));
//         $("body").css('top',top-3+'px');
//         // $("#magnify").css('left',left-0.5+'px');
//     }
// }
$(".row>.actions>.example>.checkbox-button>span").click(function(event) {
    $("a>.actions>.example>.checkbox-button>span").each(function(){
        $(this).click();
    });
});

$('.bi-calendar').parent().click(function(event) {
    $('input[name="start"]').focus();
});

$('.bi-star').click(function(event) {
    var color_now = $(this).css('color');
    color_now_hex = rgb2hex(color_now);
    if (color_now_hex == '#ffa500') {
        $(this).css('color','#999b84');
    }
    else
    {
        $(this).css('color','#ffa500');
    }
});

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

//Function to convert rgb color to hex format
function rgb2hex(rgb) {
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
 }
$('.dropdown-toggle').dropdown()